/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.peerayuth.calculateshape;

/**
 *
 * @author Ow
 */
public class Rectangle extends Shape {
    
    private double length;
    private double width;

    public Rectangle(double width,double length) {
        super("Rectangle");
        this.length = length;
        this.width = width;
    }

    public double getLength() {
        return length;
    }

    public double getWidth() {
        return width;
    }

    public void setLength(double length) {
        this.length = length;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    
    @Override
    public double calArea() {
        return width*length;
    }
    

    @Override
    public double calPerimeter() {
        return (2*width)+(2*length);
    }
    
    
}
